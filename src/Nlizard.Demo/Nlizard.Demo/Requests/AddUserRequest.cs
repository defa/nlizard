﻿using ServiceKit.Aop;
using Nlizard.Demo.Entities;
using Nlizard.Demo.Responses;
using System.ComponentModel.DataAnnotations;

namespace Nlizard.Demo.Requests
{
    public class AddUserRequest : AopRequest<AddUserResponse>
    {
        [Required(ErrorMessage = "Id是必填项")]
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
