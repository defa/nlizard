﻿using ServiceKit.Aop;

namespace Nlizard.Demo.Responses
{
    public class AddUserResponse : AopResponse
    {
        public bool Result { get; set; }
    }
}
