﻿using ServiceKit.Aop;

namespace Nlizard.Demo.Domain.Responses
{
    public class AddUserResponse : AopResponse
    {
        public bool Result { get; set; }
    }
}
