﻿using ServiceKit.Aop;
using Nlizard.Demo.Domain.Entities;
using Nlizard.Demo.Domain.Responses;
using System.ComponentModel.DataAnnotations;

namespace Nlizard.Demo.Domain.Requests
{
    public class AddUserRequest : AopRequest<AddUserResponse>
    {
        [Required(ErrorMessage = "Id是必填项")]
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
