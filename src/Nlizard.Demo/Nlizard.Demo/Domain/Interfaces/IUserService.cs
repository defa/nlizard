﻿using Nlizard.Demo.Domain.Requests;
using Nlizard.Demo.Domain.Responses;
using ServiceKit;
using ServiceKit.JsonRpc;
using System.ComponentModel;

namespace Nlizard.Demo.Domain.Intrfaces
{
    [Description("用户信息接口")]
    public interface IUserService : IAopService
    {
        [RpcMethod("com.gutun.user.adduser")]
        [Description("添加用户信息")]
        AddUserResponse AddUser(AddUserRequest request);
    }
    
}
