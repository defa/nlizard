﻿using Nlizard.Demo.Intrfaces;
using Nlizard.Demo.Requests;
using Nlizard.Demo.Responses;

namespace Nlizard.Demo.Services
{
    public class UserService : IUserService
    {
        public AddUserResponse AddUser(AddUserRequest request)
        {
            return new AddUserResponse() { Result = true, SubCode = "1", SubMsg = "保存成功" };
        }
    }
}
