﻿using ServiceKit;
using System.ComponentModel.DataAnnotations;

namespace Nlizard.Demo.Entities
{
    public class UserEntity:IEntity
    {
        [Required(ErrorMessage ="Id是必填项")]
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
