﻿using ServiceKit.Aop;

namespace Nlizard.File.Responses
{
    public class AddUserResponse : AopResponse
    {
        public bool Result { get; set; }
    }
}
