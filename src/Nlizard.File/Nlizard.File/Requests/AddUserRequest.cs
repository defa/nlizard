﻿using ServiceKit.Aop;
using Nlizard.File.Entities;
using Nlizard.File.Responses;
using System.ComponentModel.DataAnnotations;

namespace Nlizard.File.Requests
{
    public class AddUserRequest : AopRequest<AddUserResponse>
    {
        [Required(ErrorMessage = "Id是必填项")]
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
