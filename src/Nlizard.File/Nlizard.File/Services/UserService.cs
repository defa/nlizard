﻿using Nlizard.File.Intrfaces;
using Nlizard.File.Requests;
using Nlizard.File.Responses;

namespace Nlizard.File.Services
{
    public class UserService : IUserService
    {
        public AddUserResponse AddUser(AddUserRequest request)
        {
            return new AddUserResponse() { Result = true, SubCode = "1", SubMsg = "保存成功" };
        }
    }
}
