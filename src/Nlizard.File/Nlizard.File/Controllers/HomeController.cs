﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using System.Text;

namespace Nlizard.File.Controllers
{
    public class HomeController : Controller
    {
        private string key = "ValidedAopService";

        public IMemoryCache cache;

        public HomeController(IMemoryCache cache) { this.cache = cache; }

        public ActionResult Index()
        {
            var errors = cache.Get<List<string>>(key);
            if (errors != null && errors.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var item in errors)
                {
                    sb.Append(item + "<br/>");
                }
                //cache.Remove(key);
                return Content(sb.ToString(),"text/html",Encoding.UTF8);
            }
            return Content("服务已经启动!");
        }
    }
}
