﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ServiceKit.JsonRpc;
using ServiceKit.WebApi;

namespace Nlizard.File
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            RpcServerConfiguration configuration = new RpcServerConfiguration();
            var serializerSettings = new JsonSerializerSettings();
            //解决日期格式问题
            serializerSettings.Converters.Add(new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fffffff" });
            //解决Json 序列化循环引用问题.
            serializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            configuration.JsonSerializerSettings = serializerSettings;
            configuration.ModuleName = "File";
            configuration.Template = "api/{module}/{service}";
            services
                .AddMvc(opts =>
                {
                    opts.Conventions.Insert(0, new DynamicApiControllerRouteConvention(configuration));
                })
                .AddJsonRpc(configuration);

            services.AddLogging((logging) =>
             {
                 logging.AddConsole();
                 logging.AddDebug();
             });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                        name: "default",
                        template: "{controller=Home}/{action=Index}/{id?}");
            });

        }


    }
}
