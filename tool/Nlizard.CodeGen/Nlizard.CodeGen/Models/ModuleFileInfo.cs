﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TD.CodeGen.Models
{
    public class ModuleFileInfo
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("tempath")]
        public string TemPath { get; set; }
    }
}