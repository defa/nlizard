﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TD.CodeGen.Models
{
    public class ModuleInfo
    {
        [JsonProperty("keyword")]
        public string KeyWord { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("dirs")]
        public List<ModuleInfo> Dirs { get; set; }

        [JsonProperty("files")]
        public List<ModuleFileInfo> Files { get; set; }

        [JsonProperty("rootpath")]
        public string RootPath { get; set; }

        [JsonProperty("tempath")]
        public string TempPath { get; set; }
    }
}