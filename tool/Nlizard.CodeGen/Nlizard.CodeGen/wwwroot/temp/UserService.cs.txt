﻿using $$ModuleName$$.Intrfaces;
using $$ModuleName$$.Requests;
using $$ModuleName$$.Responses;

namespace $$ModuleName$$.Services
{
    public class UserService : IUserService
    {
        public AddUserResponse AddUser(AddUserRequest request)
        {
            return new AddUserResponse() { Result = true, SubCode = "1", SubMsg = "保存成功" };
        }
    }
}
