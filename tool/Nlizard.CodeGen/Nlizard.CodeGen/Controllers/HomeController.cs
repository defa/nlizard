﻿using Microsoft.AspNetCore.Mvc;

namespace Nlizard.CodeGen.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return Redirect("index.html");
        }
    }
}